# Team Members

* Jerad Hoy
* Anna Watson
* Greg Smirnow
* Sarah Hall

# Idea 1
Integrating SQLite3 Databases with Java. This guide would cover the basics of
designing tables and implementing them with SQLite3, as well as how to read and
write to the SQL database via the business logic layer of an object oriented
application. This would be useful to the reader because the usefulness of SQL
is mostly lost if your SQL database cannot interface with the application it's
used for.

# Idea 2
4 Types of Joins in SQLite3: inner, left, left outer, cross. This tutorial
would teach the different methods of joining tables, as well as when and why
each join should be used. As a side note, the guide will also cover which joins
are NOT yet supported by SQLite3. This is important for the reader to learn
because certain queries can be made MUCH more efficient by using specific types
of joins. If the wrong type of join is used, a lot of redundant work may have
to be done, or the query may return incorrect / misleading data.

# Idea 3
Converting ER Models to Relational Diagrams. This tutorial will cover how to take
an Entity Relationship (ER) Model and convert it into a valid relational diagram.
This will teach the reader what is required, the steps to take, and some of
the different cases they may run into. Readers would not only learn how to perform
this process, necessary for taking the conceptual ER Model and implementing it in a
database, but also would gain a deeper understanding of ER models and relational diagrams
themselves.
