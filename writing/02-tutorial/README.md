# Introduction to Integrating SQLite3 with Java


### Introduction to SQLite3

SQLite is a very popular system for implementing simple relational databases. These databases allow us to access, modify, and query data in a structured way. However, often we want to use a relational database within another application. In this tutorial, we will demonstrate how to integrate a simple SQLite3 database into a Java application.

To install SQLite3, follow the directions at [this link](http://www.sqlitetutorial.net/download-install-sqlite/).

### Design Database Tables

Let's say we want to model students with a relational database schema. Each column would represent one property or attribute of a student. Each row in the table would represent an individual student. In the example below we have a table containing students with the attributes ID, first name, last name, and grade. The student with the ID of 1 has the first name Sarah, the last name Hall, and is in the grade 9.


| ID    | FirstName | LastName | Grade |
| ----- | --------- | -------- | ----- |
| 0     | Jerad     | Hoy      |  10   |
| 1     | Sarah     | Hall     |  9    |
| 2     | Anna      | Watson   |  12   |
| 3     | Pep       | Boi      |  11   |



### How to Implement Tables in SQLite3

#### Creating Tables

Utilizing the ```CREATE TABLE``` command we can initialize a new table. In this example, we are creating the table above called Student into SQLite3. After the ```CREATE TABLE``` command we get to name our table. It's good practice to capitalize our table name.

```sql
CREATE TABLE Student(ID int, firstName text, lastName text, grade int);
```

#### Inserting Data into Tables

Utilizing the ```INSERT INTO``` command we can add data to our table. Following this command, we need to specify which table we would like to insert data into and then the use the```values``` command.  In this example, we are inserting four new students into our Student table that we created above. The data entered into the table must be in the order that we created the columns.



```sql
INSERT INTO Student values (0, "Jerad", "Hoy", 10);
INSERT INTO Student values (1, "Sarah", "Hall", 9);
INSERT INTO Student values (2, "Anna", "Watson", 12);
INSERT INTO Student values (3, "Pep", "Boi", 11);
```


#### How to Query the Data in SQLite
 
In order to see what tables we have in our database we use the ````.tables```` command shown below 

```sql 
.tables
```

This command will give us a list of all the tables in our database. In this example we will only retrieve our Student table shown below.

````Student````

To see the columns, entity types, and values of a select table we use the ````PRAGMA table_info```` command followed by the name of the table in parenthesis. 

```sql
PRAGMA table_info(Student);
``` 

To select all of the tuples or rows from a table we use the 
    ````SELECT * FROM````  command followed by the name of the table. For this tag we will need to add some additional information. In this example we're selecting every tuple from the Student table.
    
```sql
SELECT * FROM Student;
```

### Why SQLite should be integrated with Java

Often when designing a java application, you might run into the need to read in data from an external data source, such as a database running SQLite3.


### Structure of Java code

To connect a Java application to an SQL database, we will need to create several classes and methods to be able to model the data, read in from the database, and display our data.

The Java code used to accomplish this can be partitioned into 2 sections:
+ Data access code
+ Entity classes

An entity is another name for a Java class that represents a table in our database. For example, our database has a table called Student, so our Java code should have an Entity called student.

### Creating Entities

For each table in our database, we need to create a Java class with the same name and information as the table. In our example, we will create a new class called Student with variables for each of the columns in our table as public variables. 

```java
public class Student
{
    public int ID;
    public String firstName;
    public String lastName;
    public int grade;
    
    public Student(
        int ID,
        String firstName,
        String lastName,
        int grade)
    {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.grade = grade;
    }
}
```
Make sure to include a constructor that initializes all variables in the class (as shown above). In this example we have ID, fistName, lastName, and grade.

### Creating a Container for Entities Loaded from Database

Now that we've created out Student entity class, we need to pull data from our Student table to create Student objects in our Java code.

When a query is sent to the database via Java's built-in java.sql.Connection library, an ResultSet is returned. In order to use the data contained in this ResultSet, we need to write a class that extracts the data from the ResultSet. Let's name the class SQLTable.

SQLTable has 2 properties, columnNames and tuples:

```java
public class SQLTable
{
    private List<String> columnNames;
    private final List<List<String>> tuples;
```

The column names / talbe rows are dynamically loaded by the constructor, so this class will be able to represent the data in any SQL query's ResultSet.

The constructor accepts a ResultSet, finds the column names using the findColumnNames method, then iterates over the provided ResultSet generating a tuple from each element in the ResultSet.
```java
    public SQLTable(ResultSet resultSet)
    {
        columnNames = new ArrayList<>();
        tuples = new ArrayList<>();
            
        try
        {
            findColumnNames(resultSet);
            
            while (resultSet.next())
            {
                generateNextTuple(resultSet);
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
```

A private method named 'findColumnNames' is used to extract the column names from the ResultSet provided to this class' constructor. ResultSet.getMetaData() is used to find all the column names, which are then saved in this class' List, columnNames.

```java
    private void findColumnNames(ResultSet resultSet) throws SQLException
    {
        ResultSetMetaData setMetaData = resultSet.getMetaData();
        int columnCount = setMetaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++)
        {
            String nextColumnName = setMetaData.getColumnName(i);
            columnNames.add(nextColumnName);
        }
    }
```

In order to load an element of the ResultSet into this class' tuples List, the 'generateNextTuple' method is used. This method:
1. Gets the number of columns (lines 3 - 4)
2. Instantiates the next tuple (line 5)
3. Iterates over each column and saves the results in the tuple (lines 6 - 11)
4. Adds the tuple to this class' tuples List (line 12)

```java
    private void generateNextTuple(ResultSet resultSet) throws SQLException
    {
        ResultSetMetaData setMetaData = resultSet.getMetaData();
        int columnCount = setMetaData.getColumnCount();
        List<String> nextTuple = new ArrayList<>();
        for (int i = 0; i < columnCount; i++)
        {
            String columnName = columnNames.get(i);
            String nextValue = resultSet.getString(columnName);
            nextTuple.add(nextValue);
        }
        tuples.add(nextTuple);
    }
}
```

### Setting Up a Connection to the Database

Now that we have a Student entity class and a SQLTable class to hold the results of queries, we need a to set up a connection to the database in order to load the entities into our Java application via a SQL query. We'll create a custom class called SQLiteConnection; this class will connect to the database via Java's built-in library: java.sql.Connection.

Our SQLiteConnection class has 2 properties, connectionString and connection:

```java
public class SQLiteConnection
{
    private Connection connection;
    private String connectionString;
```

The constructor takes in the connection string used to access our database.

```java
    public SQLiteConnection(String connectionString)
    {
        this.connectionString = connectionString;
    }
```

The only 2 functionalities of this class are opening the connection, and sending a SQL query to the database.

Opening the connection is achieved via a method called 'open.'

```java
    public void open() throws SQLException
    {
        connection = DriverManager.getConnection(connectionString);
    }
```

Lastly, this class needs a method called 'executeCommand' which takes in the SQL query as a string. It then:
1. Opens the connection (line 6)
2. Executes the query and saves the result (line 8)
3. Converts the query's result to a SQLTable (line 10)
4. Closes the connection (line 12)
5. Returns the queried table (line 19)

```java
    public SQLTable executeCommand(String command)
    {
        SQLTable queriedTable = null;
        try
        {
            open();
            
            ResultSet resultSet =
                connection.createStatement().executeQuery(command);
            queriedTable = new SQLTable(resultSet);
        
            connection.close();
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
        
        return queriedTable;
    }
```

### Creating Data Retrievers

For each of the tables in our database, a class must be created to retrieve the rows from that table. Name these classes '{TableName}Retriever.' For our example, we will create the class 'StudentRetriever.'

#### Defining Properties of StudentRetriever

First, we define the SQL query that's used to retrieve all the students from the Student table. Then, we define aliases for the Student table's columns' indices (ID_INDEX = 0, etc.). Lastly, in the section we need to define an instance of SQLiteConnection. This connection will be used to send SQL commands to the database.

```java
public class StudentRetreiver
{
    private static final String QUERY_STRING = "SELECT * FROM Student;";
    private static final int ID_INDEX = 0;
    private static final int NAME_INDEX = 1;
    private static final int GRADE_INDEX = 2;
    private final SQLiteConnection SQLiteConnection;
```

#### Creating the Constructor

In this class' constructor, the connection must be instantiated as such:

```java
    public StudentRetreiver()
    {
        SQLiteConnection =
            new SQLiteConnection(DataAccessLayer.getConnectionString());
    }
```

#### Retrieving the Data

The rest of this class has a single method that returns a collection of the retrieved entity: getData. Line 3 of this method sends the SQL query defined above to the database, and gets a SQLTable in response. This SQLTable contains all the Student's in the Student table (SQLTable's implementation details are covered later in this section). 

```java
    public List<Student> getData()
    {
        SQLTable studentTable =
            SQLiteConnection.executeCommand(QUERY_STRING);
```

Then, a List of the Student entity is created. The retrieved SQLTable's tupes are iterated over, and for each tuple, the information in that tuple is used to create a new Student. This student is added to the list.
```java
        List<Student> students = new ArrayList<>();
        studentTable.getTuples().forEach(
            student ->
            {
                int id = Integer.parseInt(student.get(ID_INDEX));
                String firstName = student.get(FIRST_NAME_INDEX);
                String lastName = student.get(LAST_NAME_INDEX);
                int grade = Integer.parseInt(student.get(GRADE_INDEX));
                Student nextHighschooler = new Student(
                    id,
                    firstName,
                    lastName,
                    grade);
                
                students.add(nextHighschooler);
            });
                
            student.add(nextStudent);
        });
```
At the end of this iteration process, every tuple in the retrieved SQLTable will have been converted to a student and inserted into the Students List. This list is returned.
```java
        return students;
    }
}
```

### Creating the Data Access Layer

The data access layer consists of a class called DataAccessLayer. This class has a method called getConnectionString. This generates a string that is used to open a connection to the SQLite3 database (the connection string for Java + SQLite3 is of the format 'jdbc:sqlite:{databaseFileLocation}').

```java
public class DAL
{
    public static String getConnectionString()
    {
        String pathToDatabaseFromSource =
            "\\src\\sqlite3integration\\DatabaseFiles\\School.db";
        String currentLocation = (new File("").getAbsolutePath());
        String fullPath =
            currentLocation + pathToDatabaseFromSource;
        
        return "jdbc:sqlite:" + fullPath;
    }
```


The remainder of the class body consists of one method for each entity in the database; in this example, there's a single method for the Student entity. This method gets a List of Students from the database. It does this by creating an instance of the StudentRetriever we discussed above, and invoking its getData method.
```java
    
    public static List<Student> getStudents()
    {
        StudentRetreiver dataRetriever = new StudentRetreiver();
        return dataRetriever.getData();
    }
}
```

### Using the Data Access Layer
Now that the class DataAccessLayer is implemented with the 'getStudents' method, the rest of the Java application can access the students data stored in the database by calling ```DataAccessLater.getStudents()```.

The following code snippet is a trivial example of a Java application's driver class loading in Student data and printing the data to the console:
```java
public static void main(String[] args)
{
    System.out.println("Students");
    DataAccessLayer.getStudents().forEach(
        person ->
            System.out.println(
                "{ " + person.ID + ", " +
                person.firstName + ", " +
                person.lastName + ", " +
                person.grade + " }"));
}
```

*Written by Sarah Hall, Greg Smirnow, Anna Watson, and Jerad Hoy*